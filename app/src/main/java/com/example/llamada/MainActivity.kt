package com.example.llamada

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {
    var loadingContainer : View?= null
    var sendButton : Button?=null
    var concatenar : EditText?=null
    var input : EditText?=null
    var button1: Button?=null
    var button2: Button?=null
    var button3: Button?=null
    var button4: Button?=null
    var button5: Button?=null
    var button6: Button?=null
    var button7: Button?=null
    var button8: Button?=null
    var button9: Button?=null
    var button0: Button?=null
    var btnClear: Button?=null

    var cadena: String=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //inicializacion de botones
        button1 = findViewById(R.id.button1)
        button2 = findViewById(R.id.button2)
        button3 = findViewById(R.id.button3)
        button4 = findViewById(R.id.button4)
        button5 = findViewById(R.id.button5)
        button6 = findViewById(R.id.button6)
        button7 = findViewById(R.id.button7)
        button8 = findViewById(R.id.button8)
        button9 = findViewById(R.id.button9)
        button0 = findViewById(R.id.button0)
        btnClear = findViewById(R.id.btnClear)


        //inicializar EditText
        input = findViewById(R.id.input)


        //eventos onclick
        button1!!.setOnClickListener{view ->
            concatenar = findViewById(R.id.input)
            input!!.setText(concatenar!!.text.toString()+"1")
        }
        button2!!.setOnClickListener{view ->
            concatenar = findViewById(R.id.input)
            input!!.setText(concatenar!!.text.toString()+"2")
        }
        button3!!.setOnClickListener{view ->
            concatenar = findViewById(R.id.input)
            input!!.setText(concatenar!!.text.toString()+"3")
        }
        button4!!.setOnClickListener{view ->
            concatenar = findViewById(R.id.input)
            input!!.setText(concatenar!!.text.toString()+"4")
        }
        button5!!.setOnClickListener{view ->
            concatenar = findViewById(R.id.input)
            input!!.setText(concatenar!!.text.toString()+"5")
        }
        button6!!.setOnClickListener{view ->
            concatenar = findViewById(R.id.input)
            input!!.setText(concatenar!!.text.toString()+"6")
        }
        button7!!.setOnClickListener{view ->
            concatenar = findViewById(R.id.input)
            input!!.setText(concatenar!!.text.toString()+"7")
        }
        button8!!.setOnClickListener{view ->
            concatenar = findViewById(R.id.input)
            input!!.setText(concatenar!!.text.toString()+"8")
        }
        button9!!.setOnClickListener{view ->
            concatenar = findViewById(R.id.input)
            input!!.setText(concatenar!!.text.toString()+"9")
        }
        button0!!.setOnClickListener{view ->
            concatenar = findViewById(R.id.input)
            input!!.setText(concatenar!!.text.toString()+"0")
        }

        //Boton de llamada
        sendButton = findViewById(R.id.sendButton)

        //valida y hace la llamada solo si ha marcado numeros
        val updateHandler = Handler(Looper.getMainLooper())
        sendButton!!.setOnClickListener {
            val inputs = input!!.text.toString().trim()
            if(inputs.isNullOrBlank()){
                print("esta vacio!!")
            }else {
                showLoading(true)
                val runnable = Runnable{
                    showLoading(false)
                }
                updateHandler.postDelayed(runnable, 10000)
            }
        }


        loadingContainer = findViewById(R.id.loadingContainer)
        loadingContainer!!.setOnClickListener { showLoading(false) }
        sendButton!!.setBackgroundColor(resources.getColor(R.color.colorAccent))

        btnClear!!.setOnClickListener{
            cadena = input!!.text.toString()
            if (cadena==""||cadena.isNullOrEmpty()) print("esta vacia o null")
            else {
                cadena = cadena.substring(0,cadena.length-1)
                input!!.setText(cadena)
            }
        }
    }

    fun showLoading(show: Boolean) {
        val visibility = if(show) View.VISIBLE else View.GONE
        loadingContainer!!.visibility = visibility
    }

}
